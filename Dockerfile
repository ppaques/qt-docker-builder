FROM debian:10-slim

LABEL maintainer="Pierre Paques <pierre.paques01@gmail.com>"

#
RUN apt-get update && apt-get install --no-install-recommends -y \
    autoconf \
    automake \
    autopoint \
    bash \
    bison \
    bzip2 \
    flex \
    g++ \
    g++-multilib \
    gettext \
    git \
    gperf \
    intltool \
    libc6-dev-i386 \
    libgdk-pixbuf2.0-dev \
    libltdl-dev \
    libssl-dev \
    libtool-bin \
    libxml-parser-perl \
    lzip \
    make \
    openssl \
    p7zip-full \
    patch \
    perl \
    python \
    ruby \
    sed \
    unzip \
    wget \
    xz-utils \
    qtbase5-dev \
    libqt5serialport5-dev \
    qt5-qmake \
    qt5-default \
    && rm -rf /var/lib/apt/lists/*

RUN git clone https://github.com/yshurik/mxe.git /mxe
RUN make -C /mxe -j`nproc` qtbase qtserialport

# Enhance path
ENV PATH /mxe/usr/bin:$PATH

RUN mkdir /app
WORKDIR /app


CMD ["sh"]
